# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-08-16 19:28-0400\n"
"PO-Revision-Date: 2021-08-19 14:11+0900\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.0\n"
"Last-Translator: \n"
"Language: ko\n"

#: constants.js:77
msgid "Favorites"
msgstr "즐겨찾기"

#: constants.js:78 menulayouts/eleven.js:200 menulayouts/eleven.js:285
#: menulayouts/launcher.js:354 menulayouts/runner.js:112
#: menulayouts/tweaks/tweaks.js:1102
msgid "Frequent Apps"
msgstr "자주 사용"

#: constants.js:79 menulayouts/raven.js:57 menulayouts/raven.js:205
#: menulayouts/tweaks/tweaks.js:661 menulayouts/tweaks/tweaks.js:766
#: menulayouts/tweaks/tweaks.js:1074 menulayouts/unity.js:58
#: menulayouts/unity.js:323 menuWidgets.js:1793
msgid "All Programs"
msgstr "모든 프로그램"

#: constants.js:80 menulayouts/eleven.js:218 menulayouts/eleven.js:281
#: menulayouts/insider.js:191 menulayouts/launcher.js:398
#: menulayouts/plasma.js:140 menulayouts/raven.js:55 menulayouts/raven.js:200
#: menulayouts/raven.js:266 menulayouts/tweaks/tweaks.js:1100
#: menulayouts/unity.js:56 menulayouts/unity.js:318 menulayouts/unity.js:400
#: menulayouts/windows.js:448 menuWidgets.js:1281 prefs.js:6018 utils.js:219
msgid "Pinned Apps"
msgstr "고정된 프로그램"

#: constants.js:81 menulayouts/raven.js:288 menulayouts/unity.js:418
msgid "Recent Files"
msgstr "최근 파일"

#: constants.js:196 menulayouts/mint.js:233 menulayouts/unity.js:238
#: prefs.js:487
msgid "Log Out"
msgstr "로그아웃"

#: constants.js:197 menulayouts/mint.js:234 menulayouts/unity.js:239
#: prefs.js:486
msgid "Lock"
msgstr "잠그기"

#: constants.js:198 prefs.js:489
msgid "Restart"
msgstr "다시 시작"

#: constants.js:199 menulayouts/mint.js:235 menulayouts/unity.js:240
#: prefs.js:488
msgid "Power Off"
msgstr "컴퓨터 끄기"

#: constants.js:200 prefs.js:490
msgid "Suspend"
msgstr "대기"

#: constants.js:201 prefs.js:491
msgid "Hybrid Sleep"
msgstr "하이브리드 절전"

#: constants.js:202 prefs.js:492
msgid "Hibernate"
msgstr "절전"

#: constants.js:324 menuWidgets.js:3275 prefs.js:5822
msgid "ArcMenu"
msgstr "기본"

#: constants.js:325
msgid "Brisk"
msgstr "Brisk"

#: constants.js:326
msgid "Whisker"
msgstr "휘스커 메뉴"

#: constants.js:327
msgid "GNOME Menu"
msgstr "그놈 메뉴"

#: constants.js:328
msgid "Mint"
msgstr "리눅스 민트"

#: constants.js:329
msgid "Budgie"
msgstr "Budgie"

#: constants.js:332
msgid "Unity"
msgstr "Unity"

#: constants.js:333
msgid "Plasma"
msgstr "KDE"

#: constants.js:334
msgid "tognee"
msgstr "Tognee"

#: constants.js:335
msgid "Insider"
msgstr "인사이더"

#: constants.js:336
msgid "Redmond"
msgstr "Redmond"

#: constants.js:337
msgid "Windows"
msgstr "Windows"

#: constants.js:338
msgid "Eleven"
msgstr "Win11"

#: constants.js:341
msgid "Elementary"
msgstr "eOS 메뉴"

#: constants.js:342
msgid "Chromebook"
msgstr "크롬OS"

#: constants.js:345 constants.js:362
msgid "Launcher"
msgstr "런처"

#: constants.js:346
msgid "Runner"
msgstr "KRunner"

#: constants.js:347
msgid "GNOME Overview"
msgstr "그놈 오버뷰"

#: constants.js:350 constants.js:361
msgid "Simple"
msgstr "심플"

#: constants.js:351
msgid "Simple 2"
msgstr "심플 2"

#: constants.js:354
msgid "Raven"
msgstr "Raven"

#: constants.js:358
msgid "Traditional"
msgstr "일반적인"

#: constants.js:359
msgid "Modern"
msgstr "모던한"

#: constants.js:360
msgid "Touch"
msgstr "터치 스크린 최적화"

#: constants.js:363
msgid "Alternative"
msgstr "특이한 메뉴 스타일"

#: menuButton.js:819 menulayouts/arcmenu.js:196 menulayouts/plasma.js:186
#: menulayouts/raven.js:171 menulayouts/redmond.js:167
#: menulayouts/tognee.js:152 menulayouts/unity.js:190
#: menulayouts/windows.js:148 menuWidgets.js:2076 prefs.js:307 prefs.js:407
#: prefs.js:577 prefs.js:6133
msgid "ArcMenu Settings"
msgstr "메뉴 설정"

#: menuButton.js:825
msgid "Settings Quick Links:"
msgstr "빠른 링크 설정:"

#: menuButton.js:831
msgid "Change Menu Layout"
msgstr "메뉴 스타일 변경"

#: menuButton.js:832
msgid "Modify Pinned Apps"
msgstr "고정된 프로그램 설정"

#: menuButton.js:833
msgid "Modify Shortcuts"
msgstr "바로가기 설정"

#: menuButton.js:834
msgid "Layout Tweaks"
msgstr "메뉴 기능 개선"

#: menuButton.js:835 prefs.js:6010
msgid "Button Appearance"
msgstr "버튼 모양"

#: menuButton.js:841 prefs.js:6012
msgid "About"
msgstr "정보"

#: menuButton.js:856
msgid "Dash to Panel Settings"
msgstr "Dash to Panel 설정"

#: menuButton.js:863
msgid "Dash to Dock Settings"
msgstr "Dash to Dock 설정"

#: menuButton.js:867
msgid "Ubuntu Dock Settings"
msgstr "우분투 독 설정"

#: menulayouts/arcmenu.js:196 menulayouts/baseMenuLayout.js:486
#: menulayouts/mint.js:228 menulayouts/plasma.js:186 menulayouts/raven.js:171
#: menulayouts/redmond.js:167 menulayouts/tognee.js:152
#: menulayouts/unity.js:190 menulayouts/unity.js:233 menulayouts/windows.js:148
msgid "Software"
msgstr "소프트웨어"

#: menulayouts/arcmenu.js:196 menulayouts/mint.js:224 menulayouts/plasma.js:186
#: menulayouts/raven.js:171 menulayouts/redmond.js:167
#: menulayouts/tognee.js:152 menulayouts/unity.js:190
#: menulayouts/windows.js:148 menuWidgets.js:1259
msgid "Settings"
msgstr "설정"

#: menulayouts/arcmenu.js:196 menulayouts/plasma.js:186
#: menulayouts/raven.js:171 menulayouts/redmond.js:167
#: menulayouts/tognee.js:152 menulayouts/unity.js:190
#: menulayouts/windows.js:148
msgid "Tweaks"
msgstr "기능 개선"

#: menulayouts/arcmenu.js:196 menulayouts/mint.js:223 menulayouts/plasma.js:186
#: menulayouts/raven.js:171 menulayouts/redmond.js:167
#: menulayouts/tognee.js:152 menulayouts/unity.js:190
#: menulayouts/windows.js:148 menuWidgets.js:2079
msgid "Terminal"
msgstr "터미널"

#: menulayouts/arcmenu.js:196 menulayouts/plasma.js:186
#: menulayouts/raven.js:171 menulayouts/redmond.js:167
#: menulayouts/tognee.js:152 menulayouts/unity.js:190
#: menulayouts/windows.js:148 menuWidgets.js:810
msgid "Activities Overview"
msgstr "활동 오버뷰"

#: menulayouts/baseMenuLayout.js:428 menulayouts/baseMenuLayout.js:472
#: menulayouts/unity.js:227 placeDisplay.js:452
msgid "Home"
msgstr "홈"

#: menulayouts/baseMenuLayout.js:428 menulayouts/eleven.js:163
#: menulayouts/insider.js:73 menulayouts/mint.js:230 menulayouts/unity.js:228
#: menulayouts/windows.js:75
msgid "Documents"
msgstr "문서"

#: menulayouts/baseMenuLayout.js:428 menulayouts/eleven.js:156
#: menulayouts/unity.js:229
msgid "Downloads"
msgstr "다운로드"

#: menulayouts/baseMenuLayout.js:428
msgid "Music"
msgstr "음악"

#: menulayouts/baseMenuLayout.js:428
msgid "Pictures"
msgstr "사진"

#: menulayouts/baseMenuLayout.js:428
msgid "Videos"
msgstr "비디오"

#: menulayouts/baseMenuLayout.js:428 menulayouts/plasma.js:149
#: menulayouts/unity.js:235 placeDisplay.js:237 placeDisplay.js:261
#: prefs.js:355 prefs.js:483
msgid "Computer"
msgstr "컴퓨터"

#: menulayouts/baseMenuLayout.js:428 menulayouts/baseMenuLayout.js:480
#: menulayouts/plasma.js:336 menulayouts/windows.js:214 prefs.js:356
#: prefs.js:484
msgid "Network"
msgstr "네트워크"

#: menulayouts/baseMenuLayout.js:489 placeDisplay.js:722 prefs.js:357
#: prefs.js:485
msgid "Trash"
msgstr "휴지통"

#: menulayouts/launcher.js:130
msgid "More"
msgstr "더 보기"

#: menulayouts/launcher.js:161
msgid "All"
msgstr "모두"

#: menulayouts/launcher.js:167 menulayouts/launcher.js:352
#: menulayouts/plasma.js:145 menuWidgets.js:1601 menuWidgets.js:3158
msgid "Applications"
msgstr "프로그램"

#: menulayouts/launcher.js:298 menulayouts/launcher.js:361
msgid "Search"
msgstr "검색"

#: menulayouts/launcher.js:399 menuWidgets.js:3009 prefs.js:276
msgid "Type to search…"
msgstr "검색하려면 입력하십시오…"

#: menulayouts/mint.js:232 menulayouts/unity.js:237
msgid "Files"
msgstr "파일"

#: menulayouts/plasma.js:153 menuWidgets.js:1303
msgid "Leave"
msgstr "떠나기"

#: menulayouts/plasma.js:305 menulayouts/windows.js:289 prefs.js:6020
msgid "Application Shortcuts"
msgstr "프로그램 바로가기"

#: menulayouts/plasma.js:309 menulayouts/windows.js:293
msgid "Places"
msgstr "위치"

#: menulayouts/plasma.js:320 menulayouts/tweaks/tweaks.js:1009
#: menulayouts/tweaks/tweaks.js:1140 menulayouts/windows.js:198
msgid "Bookmarks"
msgstr "북마크"

#: menulayouts/plasma.js:328 menulayouts/windows.js:206
msgid "Devices"
msgstr "장치"

#: menulayouts/plasma.js:371 menuWidgets.js:1336
msgid "Session"
msgstr "세션"

#: menulayouts/plasma.js:380 menuWidgets.js:1346
msgid "System"
msgstr "시스템"

#: menulayouts/raven.js:268 menulayouts/unity.js:402
msgid "Shortcuts"
msgstr "바로가기"

#: menulayouts/tweaks/tweaks.js:55 menuWidgets.js:1710 menuWidgets.js:1747
#: prefs.js:2696
msgid "Back"
msgstr "뒤로"

#: menulayouts/tweaks/tweaks.js:147
msgid "Category Activation"
msgstr "분류 활성화"

#: menulayouts/tweaks/tweaks.js:153
msgid "Mouse Click"
msgstr "클릭"

#: menulayouts/tweaks/tweaks.js:154
msgid "Mouse Hover"
msgstr "올려놓기"

#: menulayouts/tweaks/tweaks.js:173
msgid "Avatar Icon Shape"
msgstr "사용자 이미지 모양"

#: menulayouts/tweaks/tweaks.js:178
msgid "Circular"
msgstr "원형"

#: menulayouts/tweaks/tweaks.js:179
msgid "Square"
msgstr "사각형"

#: menulayouts/tweaks/tweaks.js:194 menulayouts/tweaks/tweaks.js:309
msgid "Searchbar Location"
msgstr "검색창 위치"

#: menulayouts/tweaks/tweaks.js:200 menulayouts/tweaks/tweaks.js:315
msgid "Bottom"
msgstr "아래"

#: menulayouts/tweaks/tweaks.js:201 menulayouts/tweaks/tweaks.js:316
#: menulayouts/tweaks/tweaks.js:507
msgid "Top"
msgstr "위"

#: menulayouts/tweaks/tweaks.js:214
msgid "Flip Layout Horizontally"
msgstr "배치 좌우반전"

#: menulayouts/tweaks/tweaks.js:231
msgid "Disable User Avatar"
msgstr "프로필 이미지 끄기"

#: menulayouts/tweaks/tweaks.js:249
msgid "Show Applications Grid"
msgstr "프로그램을 그리드 형태로 표시"

#: menulayouts/tweaks/tweaks.js:269
msgid "Disable Frequent Apps"
msgstr "최근 프로그램 비활성화"

#: menulayouts/tweaks/tweaks.js:285
msgid "Disable Pinned Apps"
msgstr "고정됨 비활성화"

#: menulayouts/tweaks/tweaks.js:328
msgid "Activate on Hover"
msgstr "올려놓을 때 메뉴 표시"

#: menulayouts/tweaks/tweaks.js:344
msgid "Show Application Descriptions"
msgstr "프로그램 설명 표시"

#: menulayouts/tweaks/tweaks.js:360
msgid "Selected Button Border Color"
msgstr ""

#: menulayouts/tweaks/tweaks.js:378
msgid "Selected Button Background Color"
msgstr ""

#: menulayouts/tweaks/tweaks.js:397 prefs.js:1853 prefs.js:3135 prefs.js:3520
#: prefs.js:3621 prefs.js:4581 prefs.js:4730 prefs.js:4962 prefs.js:5189
msgid "Restore Defaults"
msgstr "기본값으로 초기화"

#: menulayouts/tweaks/tweaks.js:398
msgid "Restore the default settings on this page"
msgstr ""

#: menulayouts/tweaks/tweaks.js:436 menulayouts/tweaks/tweaks.js:684
#: menulayouts/tweaks/tweaks.js:800
msgid "Save"
msgstr "저장"

#: menulayouts/tweaks/tweaks.js:455
msgid "Brisk Menu Shortcuts"
msgstr "Brisk 메뉴 바로가기"

#: menulayouts/tweaks/tweaks.js:483
msgid "Enable Activities Overview Shortcut"
msgstr ""

#: menulayouts/tweaks/tweaks.js:502
msgid "Position"
msgstr "위치"

#: menulayouts/tweaks/tweaks.js:508
msgid "Centered"
msgstr "가운데"

#: menulayouts/tweaks/tweaks.js:519
msgid "Width"
msgstr "폭"

#: menulayouts/tweaks/tweaks.js:539 menulayouts/tweaks/tweaks.js:570
#: menulayouts/tweaks/tweaks.js:603 prefs.js:1744
msgid "Default"
msgstr "기본"

#: menulayouts/tweaks/tweaks.js:550
msgid "Height"
msgstr "높이"

#: menulayouts/tweaks/tweaks.js:582 prefs.js:4317
msgid "Font Size"
msgstr "글꼴 크기"

#: menulayouts/tweaks/tweaks.js:614
msgid "Show Frequent Apps"
msgstr "자주 사용 표시"

#: menulayouts/tweaks/tweaks.js:632
msgid "Inherit Shell Theme Popup Gap"
msgstr ""

#: menulayouts/tweaks/tweaks.js:655 menulayouts/tweaks/tweaks.js:760
#: menulayouts/tweaks/tweaks.js:1064 menulayouts/tweaks/tweaks.js:1091
msgid "Default Screen"
msgstr "기본 화면"

#: menulayouts/tweaks/tweaks.js:660 menulayouts/tweaks/tweaks.js:765
#: utils.js:205
msgid "Home Screen"
msgstr "홈 화면"

#: menulayouts/tweaks/tweaks.js:703
msgid "Unity Layout Buttons"
msgstr ""

#: menulayouts/tweaks/tweaks.js:712
msgid "Button Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:721 menulayouts/tweaks/tweaks.js:831
msgid "Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:731 menulayouts/tweaks/tweaks.js:841
#: prefs.js:925
msgid "None"
msgstr "없음"

#: menulayouts/tweaks/tweaks.js:742 menulayouts/tweaks/tweaks.js:852
msgid "Adjust the position of the separator in the button panel"
msgstr ""

#: menulayouts/tweaks/tweaks.js:789
msgid "Mint Layout Shortcuts"
msgstr "민트 스타일 바로가기"

#: menulayouts/tweaks/tweaks.js:822
msgid "Shortcut Separator Position"
msgstr ""

#: menulayouts/tweaks/tweaks.js:990 menulayouts/tweaks/tweaks.js:1121
msgid "External Devices"
msgstr "외부 장치"

#: menulayouts/tweaks/tweaks.js:996 menulayouts/tweaks/tweaks.js:1127
msgid "Show all connected external devices in ArcMenu"
msgstr "메뉴에 연결된 모든 외부 장치 표시"

#: menulayouts/tweaks/tweaks.js:1015 menulayouts/tweaks/tweaks.js:1146
msgid "Show all Nautilus bookmarks in ArcMenu"
msgstr "메뉴에 모든 북마크 표시"

#: menulayouts/tweaks/tweaks.js:1027 menulayouts/tweaks/tweaks.js:1158
msgid "Extra Shortcuts"
msgstr ""

#: menulayouts/tweaks/tweaks.js:1050
msgid "Nothing Yet!"
msgstr "아직 없음!"

#: menulayouts/tweaks/tweaks.js:1071
msgid "Choose the default screen for tognee Layout"
msgstr ""

#: menulayouts/tweaks/tweaks.js:1073 menulayouts/tweaks/tweaks.js:1101
msgid "Categories List"
msgstr "카테고리 목록"

#: menulayouts/tweaks/tweaks.js:1098
msgid "Choose the default screen for ArcMenu"
msgstr "메뉴를 표시할 화면 선택"

#: menulayouts/tweaks/tweaks.js:1180
msgid "Enable Weather Widget"
msgstr "날씨 위젯 사용"

#: menulayouts/tweaks/tweaks.js:1198
msgid "Enable Clock Widget"
msgstr "시계 위젯 사용"

#: menulayouts/windows.js:383
msgid "Frequent"
msgstr "자주 사용"

#: menuWidgets.js:131
msgid "Current Windows:"
msgstr "현재 창:"

#: menuWidgets.js:152
msgid "New Window"
msgstr "새 창"

#: menuWidgets.js:165
msgid "Launch using Integrated Graphics Card"
msgstr ""

#: menuWidgets.js:166
msgid "Launch using Discrete Graphics Card"
msgstr ""

#: menuWidgets.js:210
msgid "Delete Desktop Shortcut"
msgstr ""

#: menuWidgets.js:223
msgid "Create Desktop Shortcut"
msgstr "바탕 화면에 바로가기 추가"

#: menuWidgets.js:244
msgid "Remove from Favorites"
msgstr ""

#: menuWidgets.js:251
msgid "Add to Favorites"
msgstr "즐겨찾기에 추가"

#: menuWidgets.js:273 menuWidgets.js:335
msgid "Unpin from ArcMenu"
msgstr "고정 해제"

#: menuWidgets.js:286
msgid "Pin to ArcMenu"
msgstr "고정하기"

#: menuWidgets.js:298
msgid "Show Details"
msgstr "자세히 보기"

#: menuWidgets.js:317
msgid "Open Folder Location"
msgstr "폴더 위치 열기"

#: menuWidgets.js:1269
msgid "Configure Runner Layout"
msgstr ""

#: menuWidgets.js:1292
msgid "Extras"
msgstr "그 외"

#: menuWidgets.js:1394
msgid "Categories"
msgstr "카테고리"

#: menuWidgets.js:1404
msgid "Users"
msgstr "사용자"

#: menuWidgets.js:1661 menuWidgets.js:1678
msgid "All Apps"
msgstr "모든 프로그램"

#: menuWidgets.js:2356
msgid "New"
msgstr "추가됨"

#: menuWidgets.js:3263
msgid "Show Applications"
msgstr "프로그램 보기"

#: menuWidgets.js:3469
msgid "Add world clocks…"
msgstr "세계 시계 추가…"

#: menuWidgets.js:3470
msgid "World Clocks"
msgstr "세계 시계"

#: menuWidgets.js:3578
msgid "Weather"
msgstr "날씨"

#: menuWidgets.js:3698
msgid "Select a location…"
msgstr "위치 선택…"

#: menuWidgets.js:3711
msgid "Loading…"
msgstr "불러오는 중…"

#: menuWidgets.js:3721
msgid "Go online for weather information"
msgstr "날씨 정보를 온라인에서 보기"

#: menuWidgets.js:3723
msgid "Weather information is currently unavailable"
msgstr "날씨 정보를 사용할 수 없음"

#: placeDisplay.js:138
#, javascript-format
msgid "Failed to launch “%s”"
msgstr "\"%s\"(을)를 실행할 수 없습니다"

#: placeDisplay.js:153
#, javascript-format
msgid "Failed to mount volume for “%s”"
msgstr "\"%s\" 드라이브를 마운트하지 못했습니다"

#: placeDisplay.js:326
#, javascript-format
msgid "Ejecting drive “%s” failed:"
msgstr "\"%s\" 드라이브를 꺼내지 못했습니다"

#: placeDisplay.js:732 placeDisplay.js:738
msgid "Empty Trash"
msgstr "휴지통 비우기"

#: prefs.js:72 prefs.js:749 prefs.js:1385 prefs.js:1589 prefs.js:2683
#: prefs.js:3167 prefs.js:3553 prefs.js:3624 prefs.js:3927 prefs.js:4632
#: prefs.js:4733 prefs.js:4965 prefs.js:5192
msgid "Apply"
msgstr "적용"

#: prefs.js:85 prefs.js:4975
msgid "Add More Apps"
msgstr "더 많은 프로그램 추가"

#: prefs.js:122 prefs.js:4778 prefs.js:5010
msgid "Add Custom Shortcut"
msgstr "커스텀 바로가기 추가"

#: prefs.js:263
msgid "Add to your Pinned Apps"
msgstr "고정된 프로그램에서 추가"

#: prefs.js:265
msgid "Change Selected Pinned App"
msgstr ""

#: prefs.js:267
msgid "Select Application Shortcuts"
msgstr "프로그램 바로가기 선택"

#: prefs.js:269
msgid "Select Directory Shortcuts"
msgstr ""

#: prefs.js:336 prefs.js:749
msgid "Add"
msgstr "추가"

#: prefs.js:408 prefs.js:1336
msgid "Run Command..."
msgstr "명령 실행..."

#: prefs.js:409
msgid "Show All Applications"
msgstr "모든 프로그램 표시"

#: prefs.js:458
msgid "Default Apps"
msgstr "기본 프로그램"

#: prefs.js:462 prefs.js:542
msgid "System Apps"
msgstr "시스템 프로그램"

#: prefs.js:538
msgid "Presets"
msgstr "프리셋"

#: prefs.js:657 prefs.js:659
msgid "Edit Pinned App"
msgstr "고정된 프로그램 편집"

#: prefs.js:657 prefs.js:659 prefs.js:661 prefs.js:663
msgid "Add a Custom Shortcut"
msgstr "커스텀 바로가기 추가"

#: prefs.js:661
msgid "Edit Shortcut"
msgstr "바로가기 편집"

#: prefs.js:663
msgid "Edit Custom Shortcut"
msgstr "커스텀 바로가기 편집"

#: prefs.js:670
msgid "Shortcut Name:"
msgstr "이름:"

#: prefs.js:685
msgid "Icon:"
msgstr "아이콘:"

#: prefs.js:697
msgid "Browse..."
msgstr "찾기..."

#: prefs.js:702 prefs.js:2153 prefs.js:2157
msgid "Select an Icon"
msgstr "아이콘 선택"

#: prefs.js:734
msgid "Terminal Command:"
msgstr "명령:"

#: prefs.js:741
msgid "Shortcut Path:"
msgstr "바로가기 위치:"

#: prefs.js:800
msgid "Panel and Dock Options"
msgstr "패널과 독 옵션"

#: prefs.js:817
msgid "Activities Hot Corner Options"
msgstr "핫 코너 동작 옵션"

#: prefs.js:827 prefs.js:1285
msgid "Modify Activities Hot Corner"
msgstr "핫 코너 동작 편집"

#: prefs.js:864
msgid "Hotkey activation"
msgstr "단축키 활성화"

#: prefs.js:872
msgid "Key Release"
msgstr "키를 떼십시오"

#: prefs.js:873
msgid "Key Press"
msgstr "키를 누르십시오"

#: prefs.js:889
msgid "Hotkey Options"
msgstr "단축키 설정"

#: prefs.js:899
msgid "Choose a Hotkey for ArcMenu"
msgstr "메뉴를 표시할 때 사용할 단축키 선택"

#: prefs.js:908
msgid "Left Super Key"
msgstr "왼쪽 Super"

#: prefs.js:913
msgid "Right Super Key"
msgstr "오른쪽 Super"

#: prefs.js:919
msgid "Custom Hotkey"
msgstr "단축키 설정"

#: prefs.js:988
msgid "Current Hotkey"
msgstr "현재 단축키"

#: prefs.js:1001
msgid "Modify Hotkey"
msgstr "단축키 편집"

#: prefs.js:1037
msgid "Display ArcMenu On"
msgstr "메뉴를 표시할 곳"

#: prefs.js:1047
msgid "Dash to Dock"
msgstr "Dash to Dock"

#: prefs.js:1051
msgid "Ubuntu Dock"
msgstr "Ubuntu Dock"

#: prefs.js:1054
msgid "Main Panel"
msgstr "상단 패널"

#: prefs.js:1055
msgid "Dash to Panel"
msgstr "Dash to Panel"

#: prefs.js:1064
msgid "Dash to Dock extension not running!"
msgstr "Dash to Dock 확장이 꺼져 있습니다!"

#: prefs.js:1064
msgid "Enable Dash to Dock for this feature to work."
msgstr "이 기능이 작동하려면 Dash to Dock을 활성화해야 합니다."

#: prefs.js:1072
msgid "Show Activities Button"
msgstr "활동 버튼 표시"

#: prefs.js:1106
msgid "Main Panel not found!"
msgstr "상단 패널을 찾을 수 없습니다!"

#: prefs.js:1107
msgid "Dash to Panel extension not running!"
msgstr "Dash to Panel 확장이 꺼져 있습니다!"

#: prefs.js:1107
msgid "Enable Dash to Panel for this feature to work."
msgstr "이 기능이 작동하려면 Dash to Panel을 활성화해야 합니다."

#: prefs.js:1127
msgid "Position in Panel"
msgstr "패널 내 위치"

#: prefs.js:1134 prefs.js:1175
msgid "Left"
msgstr "왼쪽"

#: prefs.js:1137 prefs.js:1176
msgid "Center"
msgstr "가운데"

#: prefs.js:1141 prefs.js:1177
msgid "Right"
msgstr "오른쪽"

#: prefs.js:1165
msgid "Menu Alignment"
msgstr "메뉴 배치"

#: prefs.js:1188
msgid "Display ArcMenu on all monitors"
msgstr "모든 모니터에 ArcMenu 표시"

#: prefs.js:1293
msgid "Activities Hot Corner Action"
msgstr "핫 코너 동작 활동"

#: prefs.js:1301
msgid "GNOME Default"
msgstr "그놈 기본값"

#: prefs.js:1302
msgid "Disabled"
msgstr "끄기"

#: prefs.js:1303
msgid "Toggle ArcMenu"
msgstr "ArcMenu 토글"

#: prefs.js:1304
msgid "Custom"
msgstr "그 외"

#: prefs.js:1310
msgid "Custom Activities Hot Corner Action"
msgstr "핫 코너 동작 개인화"

#: prefs.js:1310
msgid "Choose from a list of preset commands or use your own terminal command"
msgstr ""

#: prefs.js:1320
msgid "Preset commands"
msgstr "명령어 프리셋"

#: prefs.js:1329
msgid "Show all Applications"
msgstr "모든 프로그램을 표시"

#: prefs.js:1330
msgid "GNOME Terminal"
msgstr "터미널"

#: prefs.js:1331
msgid "GNOME System Monitor"
msgstr "시스템 감시"

#: prefs.js:1332
msgid "GNOME Calculator"
msgstr "계산기"

#: prefs.js:1333
msgid "GNOME gedit"
msgstr "텍스트 편집기"

#: prefs.js:1334
msgid "GNOME Screenshot"
msgstr "스크린샷"

#: prefs.js:1335
msgid "GNOME Weather"
msgstr "날씨"

#: prefs.js:1360
msgid "Terminal Command"
msgstr "터미널 명령어"

#: prefs.js:1457
msgid "Set Custom Hotkey"
msgstr "바로가기 키 설정"

#: prefs.js:1481
msgid "Choose Modifiers"
msgstr ""

#: prefs.js:1497
msgid "Ctrl"
msgstr "Ctrl"

#: prefs.js:1500
msgid "Super"
msgstr "Super"

#: prefs.js:1503
msgid "Shift"
msgstr "Shift"

#: prefs.js:1506
msgid "Alt"
msgstr "Alt"

#: prefs.js:1557
msgid "Press any key"
msgstr "아무 키나 누르십시오"

#: prefs.js:1574
msgid "New Hotkey"
msgstr "새 바로가기"

#: prefs.js:1652
msgid "Menu Button Appearance"
msgstr "메뉴 버튼 모양"

#: prefs.js:1662
msgid "Appearance"
msgstr "모양"

#: prefs.js:1668 prefs.js:1790
msgid "Icon"
msgstr "아이콘"

#: prefs.js:1669 prefs.js:1759
msgid "Text"
msgstr "텍스트"

#: prefs.js:1670
msgid "Icon and Text"
msgstr "아이콘과 텍스트"

#: prefs.js:1671
msgid "Text and Icon"
msgstr "텍스트와 아이콘"

#: prefs.js:1672
msgid "Hidden"
msgstr "숨기기"

#: prefs.js:1701
msgid "Show Arrow"
msgstr "화살표 표시"

#: prefs.js:1721
msgid "Menu Button Padding"
msgstr "메뉴 버튼 위치"

#: prefs.js:1780
msgid "Icon Appearance"
msgstr "아이콘 모양"

#: prefs.js:1797
msgid "Browse Icons"
msgstr "아이콘 둘러보기"

#: prefs.js:1817
msgid "Icon Size"
msgstr "아이콘 크기"

#: prefs.js:1861
msgid "Menu Button Styling"
msgstr "메뉴 버튼 스타일"

#: prefs.js:1872
msgid "Color"
msgstr "일반"

#: prefs.js:1879
msgid "Hover Color"
msgstr "올려놓을 때"

#: prefs.js:1886
msgid "Active Color"
msgstr "활성 상태"

#: prefs.js:1893
msgid "Hover Background Color"
msgstr "올려놓을 때 배경 색상"

#: prefs.js:1900
msgid "Active Background Color"
msgstr "활성 상태일 때 배경 색상"

#: prefs.js:1908
msgid "Disable Rounded Corners"
msgstr "둥근 모서리 비활성화"

#: prefs.js:2056 prefs.js:2192
msgid "ArcMenu Icons"
msgstr "ArcMenu 아이콘"

#: prefs.js:2144
msgid "Browse for a Custom Icon"
msgstr "커스텀 아이콘 찾기"

#: prefs.js:2193
msgid "Distro Icons"
msgstr "배포판 아이콘"

#: prefs.js:2194
msgid "Custom Icon"
msgstr "직접 설정"

#: prefs.js:2259
msgid "Legal disclaimer for Distro Icons..."
msgstr ""

#: prefs.js:2341
msgid "Current Menu Layout"
msgstr "현재 메뉴 스타일"

#: prefs.js:2347
msgid "Available Menu Layouts"
msgstr "사용 가능한 메뉴 스타일"

#: prefs.js:2477
#, javascript-format
msgid "%s Layout Tweaks"
msgstr "%s 스타일 설정"

#: prefs.js:2535
msgid "Enable Custom Menu Theme"
msgstr "개인화된 메뉴 테마 사용"

#: prefs.js:2545
msgid "Override Menu Theme"
msgstr "메뉴 테마 무시"

#: prefs.js:2705 prefsWidgets.js:769
#, javascript-format
msgid "%s Menu Layouts"
msgstr "%s 메뉴 스타일"

#: prefs.js:2798
msgid "Menu Height"
msgstr "메뉴 높이"

#: prefs.js:2851
msgid "Left-Panel Width"
msgstr "왼쪽 패널 폭"

#: prefs.js:2896
msgid "Right-Panel Width"
msgstr "오른쪽 패널 폭"

#: prefs.js:2943
msgid "Override Menu Location"
msgstr "메뉴 위치 무시"

#: prefs.js:2952
msgid "Off"
msgstr "끄기"

#: prefs.js:2953
msgid "Top Centered"
msgstr ""

#: prefs.js:2954
msgid "Bottom Centered"
msgstr ""

#: prefs.js:2969
msgid "Large Application Icons"
msgstr "큰 프로그램 아이콘"

#: prefs.js:2990
msgid "Category Sub Menus"
msgstr "분류 하위 메뉴"

#: prefs.js:3013
msgid "Disable Tooltips"
msgstr "툴팁 사용 안 함"

#: prefs.js:3037 prefs.js:4529
msgid "Enable Vertical Separator"
msgstr "세로 분리선 사용"

#: prefs.js:3058 prefs.js:4551
msgid "Separator Color"
msgstr "분리선 색상"

#: prefs.js:3083
msgid "Disable Recently Installed Apps Indicator"
msgstr "최근 설치한 프로그램 인디케이터 표시하지 않기"

#: prefs.js:3105
msgid "Clear all Applications Marked \"New\""
msgstr ""

#: prefs.js:3113
msgid "Clear All"
msgstr "모두 제거"

#: prefs.js:3245
msgid "Disable Category Arrows"
msgstr "분류 화살표 없애기"

#: prefs.js:3268
msgid "Disable Searchbox Border"
msgstr "검색창 테두리 없애기"

#: prefs.js:3291
msgid "Disable Menu Arrow"
msgstr "메뉴 화살표 없애기"

#: prefs.js:3314
msgid "Disable ScrollView Fade Effects"
msgstr "스크롤 표시 페이드 효과 끄기"

#: prefs.js:3337
msgid "Search Results - Show Descriptions"
msgstr "검색 결과 - 설명 표시"

#: prefs.js:3357
msgid "Alphabetize 'All Programs' Category"
msgstr "모든 프로그램 카테고리를 알파벳 순으로 정렬"

#: prefs.js:3377 prefs.js:3394
msgid "Multi-Lined Labels"
msgstr ""

#: prefs.js:3395
msgid "Enable/Disable multi-lined labels on large application icon layouts."
msgstr ""

#: prefs.js:3411
msgid "Recently Installed Application Indicators"
msgstr ""

#: prefs.js:3422
msgid "Category Indicator Color"
msgstr "카테고리 표시 색상"

#: prefs.js:3445
msgid "Application Indicator Label Color"
msgstr ""

#: prefs.js:3470
msgid "Gap Adjustment"
msgstr ""

#: prefs.js:3498
msgid "Offset ArcMenu by 1px"
msgstr ""

#: prefs.js:3499
msgid "Useful if you notice a 1px gap or overlap between ArcMenu and the panel"
msgstr ""

#: prefs.js:3747
msgid "Color Theme Name"
msgstr "색상 테마 이름"

#: prefs.js:3753
msgid "Name:"
msgstr "이름:"

#: prefs.js:3777
msgid "Save Theme"
msgstr "테마 저장"

#: prefs.js:3801
msgid "Select Themes to Import"
msgstr "불러올 테마 선택"

#: prefs.js:3801
msgid "Select Themes to Export"
msgstr "내보낼 테마 선택"

#: prefs.js:3817
msgid "Import"
msgstr "불러오기"

#: prefs.js:3817
msgid "Export"
msgstr "내보내기"

#: prefs.js:3846
msgid "Select All"
msgstr "모두 선택"

#: prefs.js:3913 prefs.js:4189
msgid "Manage Presets"
msgstr "프리셋 관리"

#: prefs.js:4074 prefs.js:5427
msgid "Menu Theme Presets"
msgstr "메뉴 테마 프리셋"

#: prefs.js:4084
msgid "Current Menu Theme Preset"
msgstr "현재 프리셋"

#: prefs.js:4104
msgid "Save as Preset"
msgstr "프리셋으로 저장"

#: prefs.js:4205
msgid "Browse Presets"
msgstr "프리셋 둘러보기"

#: prefs.js:4254
msgid "Theme Settings"
msgstr "테마 설정"

#: prefs.js:4274
msgid "Menu Background Color"
msgstr "메뉴 배경 색"

#: prefs.js:4296
msgid "Menu Foreground Color"
msgstr "메뉴 전경 색"

#: prefs.js:4347
msgid "Border Color"
msgstr "테두리 색상"

#: prefs.js:4369
msgid "Border Size"
msgstr "테두리 크기"

#: prefs.js:4398
msgid "Active Item Background Color"
msgstr "활성 항목 배경"

#: prefs.js:4420
msgid "Active Item Foreground Color"
msgstr "활성 항목 전경"

#: prefs.js:4442
msgid "Corner Radius"
msgstr "모서리 반지름"

#: prefs.js:4471
msgid "Menu Arrow Size"
msgstr "메뉴 화살표 크기"

#: prefs.js:4500
msgid "Menu Displacement"
msgstr ""

#: prefs.js:4743
msgid "Add Default User Directories"
msgstr "기본 사용자 위치 추가"

#: prefs.js:5330
msgid "Export or Import Settings"
msgstr "설정 내보내거나 불러오기"

#: prefs.js:5340
msgid "All ArcMenu Settings"
msgstr "모든 ArcMenu 설정"

#: prefs.js:5351
msgid "Export or Import All ArcMenu Settings"
msgstr "모든 설정을 내보내거나 불러옵니다."

#: prefs.js:5352
msgid ""
"Importing settings from file may replace ALL saved settings.\n"
"This includes all saved pinned apps."
msgstr ""
"설정을 파일에서 불러오면 기존에 저장된 설정은 사라집니다.\n"
"기존에 있던 고정된 프로그램도 포함됩니다."

#: prefs.js:5362
msgid "Import from File"
msgstr "파일에서 불러오기"

#: prefs.js:5368
msgid "Import settings"
msgstr "설정 불러오기"

#: prefs.js:5397
msgid "Export to File"
msgstr "파일로 내보내기"

#: prefs.js:5403
msgid "Export settings"
msgstr "설정 내보내기"

#: prefs.js:5438
msgid "Export or Import Menu Theme Presets"
msgstr "설정을 내보내거나 불러옵니다"

#: prefs.js:5439
msgid "Menu theme presets are located in the \"Menu Theme\" section"
msgstr "메뉴 테마 프리셋은 \"메뉴 테마\" 항목에 위치해 있습니다"

#: prefs.js:5449 prefs.js:5455
msgid "Import Theme Preset"
msgstr "테마 프리셋 불러오기"

#: prefs.js:5491 prefs.js:5502
msgid "Export Theme Preset"
msgstr "테마 프리셋 내보내기"

#: prefs.js:5534
msgid "ArcMenu Settings Window Size"
msgstr "메뉴 설정 창 크기"

#: prefs.js:5544
msgid "Window Width"
msgstr "창 너비"

#: prefs.js:5568
msgid "Window Height"
msgstr "창 높이"

#: prefs.js:5597
msgid "Reset all Settings"
msgstr "모든 설정 초기화"

#: prefs.js:5603
msgid "Restore Default Settings?"
msgstr "기본 설정으로 되돌리시겠습니까?"

#: prefs.js:5603
msgid "All ArcMenu settings will be reset to the default value."
msgstr "모든 설정이 기본값으로 초기화됩니다."

#: prefs.js:5710
msgid "ArcMenu Version"
msgstr "ArcMenu 버전"

#: prefs.js:5727
msgid "Git Commit"
msgstr "Git 커밋"

#: prefs.js:5747
msgid "GNOME Version"
msgstr "그놈 버전"

#: prefs.js:5765
msgid "OS"
msgstr "운영체제"

#: prefs.js:5797
msgid "Session Type"
msgstr "세션 유형"

#: prefs.js:5829
msgid "Application Menu Extension for GNOME"
msgstr "그놈용 프로그램 메뉴 확장"

#: prefs.js:5881
msgid "Developers"
msgstr "개발"

#: prefs.js:5883
msgid "Translators"
msgstr "번역"

#: prefs.js:5885
msgid "Contributors"
msgstr "기여"

#: prefs.js:5887
msgid "Artwork"
msgstr "아트워크"

#: prefs.js:5889
msgid "Patrons"
msgstr "Patrons"

#: prefs.js:6006
msgid "General"
msgstr "일반"

#: prefs.js:6007
msgid "Menu Layout"
msgstr "메뉴 스타일"

#: prefs.js:6008
msgid "Menu Theme"
msgstr "메뉴 테마"

#: prefs.js:6009
msgid "Customize Menu"
msgstr "메뉴 개인화"

#: prefs.js:6011
msgid "Misc"
msgstr "기타"

#: prefs.js:6017
msgid "Menu Settings"
msgstr "메뉴 설정"

#: prefs.js:6019
msgid "Directory Shortcuts"
msgstr "위치 바로가기"

#: prefs.js:6021
msgid "Power Options"
msgstr "전원 옵션"

#: prefs.js:6022
msgid "Extra Categories"
msgstr "기타 분류"

#: prefs.js:6023
msgid "Fine-Tune"
msgstr "파인 튠"

#: prefs.js:6144
msgid "Invalid Shortcut"
msgstr "알 수 없는 바로가기"

#: prefsWidgets.js:481
msgid "Modify"
msgstr "편집"

#: prefsWidgets.js:514
msgid "Move Up"
msgstr "올리기"

#: prefsWidgets.js:530
msgid "Move Down"
msgstr "내리기"

#: prefsWidgets.js:547
msgid "Delete"
msgstr "삭제"

#: search.js:664
msgid "Searching..."
msgstr "검색 중..."

#: search.js:666
msgid "No results."
msgstr "결과가 없습니다."

#: search.js:799
#, javascript-format
msgid "%d more"
msgid_plural "%d more"
msgstr[0] "%d 외"

#: utils.js:59
msgid "ArcMenu - Hybrid Sleep Error!"
msgstr "하이브리드 절전 실패!"

#: utils.js:59
msgid "System unable to hybrid sleep."
msgstr "하이브리드 절전을 할 수 없습니다."

#: utils.js:76
msgid "ArcMenu - Hibernate Error!"
msgstr "절전 실패!"

#: utils.js:76
msgid "System unable to hibernate."
msgstr "절전을 할 수 없습니다."

# Found in ArcMenu for GNOME shell 3.36/3.38
msgid "Disable Active Indicator"
msgstr "활동 표시기 끄기"

# Found in ArcMenu for GNOME shell 3.36/3.38
msgid "Launch using Dedicated Graphics Card"
msgstr ""
